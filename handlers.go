package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type Handlers struct {
	usecase    *Usecase
	ttlDefault time.Duration
}

func NewHandlers(usecase *Usecase, ttlDefault time.Duration) *Handlers {
	return &Handlers{
		usecase:    usecase,
		ttlDefault: ttlDefault,
	}
}

func (h Handlers) HandlePicture(writer http.ResponseWriter, request *http.Request) {
	v := request.URL.Query()
	urls := v.Get("url")
	width := v.Get("width")
	height := v.Get("height")
	_, err := url.PathUnescape(urls)
	if err != nil {
		log.Printf("error: %s", err)
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	imageFile, err := NewImage(urls, width, height)
	if err != nil {
		log.Printf("error: %s", err)
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	imageInCache, err := h.usecase.ReceivedFile(imageFile)
	if err != nil {
		log.Printf("usecase error: %s", err)
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	dat, err := ioutil.ReadFile(imageInCache.fileName)
	if err != nil {
		log.Printf("readfile error: %s", err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	img, _, err := image.Decode(bytes.NewReader(dat))
	if err != nil {
		log.Printf("decode error: %s", err)
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = h.writeImage(writer, &img, imageInCache.timeCache)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	}

}

func (h Handlers) writeImage(w http.ResponseWriter, img *image.Image, timecache time.Time) error {
	buffer := new(bytes.Buffer)
	if err := jpeg.Encode(buffer, *img, nil); err != nil {
		return fmt.Errorf("unable to encode image.")
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		return fmt.Errorf("unable to write image.")
	}

	timeAge := fmt.Sprintf("max-age=%d", int(timecache.Add(h.ttlDefault).Sub(time.Now()).Seconds()))
	w.Header().Set("Cache-Control", timeAge)
	return nil
}
