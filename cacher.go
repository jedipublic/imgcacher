package main

import (
	"context"
	"sort"
	"time"
)

type Func func(key string) (interface{}, error)
type FuncDone func(value interface{})

type result struct {
	value interface{}
	err   error
}

type request struct {
	key             string
	response        chan<- result
	makeCacheFunc   Func
	removeCacheFunc FuncDone
}

type entry struct {
	res           result
	ready         chan struct{}
	funcDoneCache FuncDone
}

func (e *entry) done() {
	e.funcDoneCache(e.res.value)
}

func (e *entry) deliver(response chan<- result) {
	<-e.ready
	response <- e.res
}

func (e *entry) call(f Func, key string) {
	e.res.value, e.res.err = f(key)
	close(e.ready)
}

type Memo struct {
	cache       map[string]*entry
	request     chan request
	removeKey   chan string
	ttlDuration time.Duration
	invalidator *Invalidator
}

func NewMemo(ctx context.Context, ttlDuration, ttlInvalidate time.Duration) *Memo {
	removeKey := make(chan string)
	invalidator := NewInvalidatro(removeKey, ttlInvalidate)
	memo := &Memo{
		request:     make(chan request),
		cache:       make(map[string]*entry),
		removeKey:   removeKey,
		invalidator: invalidator,
		ttlDuration: ttlDuration,
	}
	go memo.server(ctx)
	go memo.invalidator.Run(ctx)
	return memo
}

func (m *Memo) Get(key string, makeCache Func, removeCache FuncDone) (interface{}, error) {
	response := make(chan result)
	m.request <- request{
		key:             key,
		response:        response,
		makeCacheFunc:   makeCache,
		removeCacheFunc: removeCache,
	}

	res := <-response
	return res.value, res.err
}

func (m *Memo) Close() {
	close(m.request)
}

func (m *Memo) server(ctx context.Context) {
	for {
		select {
		case req := <-m.request:
			e := m.cache[req.key]
			if e == nil {
				e = &entry{
					ready:         make(chan struct{}),
					funcDoneCache: req.removeCacheFunc,
				}
				m.cache[req.key] = e

				m.invalidator.Add(req.key, time.Now().Add(m.ttlDuration))

				go e.call(req.makeCacheFunc, req.key)

			}
			go e.deliver(req.response)
		case key := <-m.removeKey:
			e := m.cache[key]
			if e == nil {
				continue
			}
			e.funcDoneCache(e.res.value)
			delete(m.cache, key)
		case <-ctx.Done():
			return
		}
	}
}

type keyttl struct {
	key string
	ttl time.Time
}

type Invalidator struct {
	elem          []keyttl
	tick          *time.Ticker
	cmdAddKeyTtl  chan keyttl
	memoKeyRemove chan string
}

func NewInvalidatro(memoKeyRemove chan string, invalidate time.Duration) *Invalidator {
	return &Invalidator{
		cmdAddKeyTtl:  make(chan keyttl),
		tick:          time.NewTicker(invalidate),
		memoKeyRemove: memoKeyRemove,
	}
}

func (i *Invalidator) Add(key string, ttl time.Time) {
	i.cmdAddKeyTtl <- keyttl{key: key, ttl: ttl}
}

func (i *Invalidator) Run(ctx context.Context) {
	for {
		select {

		case add := <-i.cmdAddKeyTtl:
			i.elem = append(i.elem, keyttl{ttl: add.ttl, key: add.key})
			sort.Sort(i)

		case <-i.tick.C:
			i.CheckOnlyLastExpiredCache()
		case <-ctx.Done():
			return
		}
	}
}

func (i *Invalidator) CheckOnlyLastExpiredCache() {
	for _, e := range i.elem {
		if e.ttl.After(time.Now()) {
			return
		}

		if e.ttl.Before(time.Now()) {
			i.memoKeyRemove <- e.key
			i.elem = i.elem[1:]
		}
	}
}

func (i *Invalidator) Len() int {
	return len(i.elem)
}

func (i *Invalidator) Swap(x, y int) {
	i.elem[x], i.elem[y] = i.elem[y], i.elem[x]
}

func (i *Invalidator) Less(x, y int) bool {
	return i.elem[x].ttl.Before(i.elem[y].ttl)
}
