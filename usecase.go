package main

type NonBlockedCacher interface {
	Get(key string, makeCache Func, removeCache FuncDone) (interface{}, error)
}

type Usecase struct {
	memoCache NonBlockedCacher
}

func NewUsecase(memoCache NonBlockedCacher) *Usecase {
	return &Usecase{
		memoCache: memoCache,
	}
}

func (u Usecase) ReceivedFile(image *Image) (ImageInCache, error) {
	key := image.UniqKey()
	fileIface, err := u.memoCache.Get(key, image.DownloadResize, image.RemoveFile)
	if err != nil {
		return ImageInCache{}, err
	}

	return fileIface.(ImageInCache), nil
}
