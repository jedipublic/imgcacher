package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/nfnt/resize"
)

type Image struct {
	url    string
	width  uint
	height uint
}

type ImageInCache struct {
	fileName  string
	timeCache time.Time
}

func (i Image) UniqKey() string {
	r := append([]byte(i.url), byte(i.height%256), byte(i.width%256))
	sum := sha256.Sum256(r)
	return fmt.Sprintf("%x", sum)
}

func NewImage(urlImage, width, height string) (image *Image, err error) {
	urlValid, err := urlParam(urlImage)
	if err != nil {
		return nil, err
	}

	widthValid, err := uintParam(width)
	if err != nil {
		return nil, err
	}

	heightValid, err := uintParam(height)
	if err != nil {
		return nil, err
	}

	return &Image{
		url:    urlValid,
		width:  widthValid,
		height: heightValid,
	}, nil
}

func (i Image) RemoveFile(file interface{}) {
	if file == nil {
		log.Printf("nil try to remove %v", file)
		return
	}
	fimeCache := file.(ImageInCache)
	err := os.Remove(fimeCache.fileName)
	if err != nil {
		log.Printf("Unable to remove file: %s", fimeCache.fileName)
	}
}

func (i Image) DownloadResize(key string) (fileMe interface{}, err error) {
	response, err := http.Get(i.url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if err != nil {
		return nil, err
	}

	fileName := fmt.Sprintf("img/%s.jpg", key)
	file, err := os.Create(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	imageRaw, _, err := image.Decode(bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	newwImage := resize.Resize(i.width, i.height, imageRaw, resize.Lanczos3)

	buff := new(bytes.Buffer)
	err = png.Encode(buff, newwImage)
	if err != nil {
		fmt.Println("failed to create buffer", err)
	}

	_, err = io.Copy(file, buff)
	if err != nil {
		return nil, err
	}

	return ImageInCache{
		fileName:  fileName,
		timeCache: time.Now(),
	}, nil
}

func uintParam(size string) (uint, error) {
	u64, err := strconv.ParseUint(size, 10, 32)
	if err != nil {
		return 0, err
	}
	return uint(u64), nil
}

func urlParam(urlImage string) (string, error) {
	u, err := url.ParseRequestURI(urlImage)
	if err != nil {
		return "", err
	}

	return u.String(), nil
}
