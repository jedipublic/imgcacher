package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Serve struct {
	addr       string
	router     *mux.Router
	serverHTTP http.Server
}

func NewServe(port uint, routes ...Route) *Serve {
	addr := fmt.Sprintf(":%d", port)
	log.Printf("start http server: %s", addr)
	return &Serve{
		addr:       addr,
		router:     register(routes...),
		serverHTTP: http.Server{Addr: addr},
	}
}

func register(routes ...Route) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		handler := logRequest(route.HandlerFunc, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).Handler(handler)
	}

	fs := http.FileServer(http.Dir("./"))
	router.PathPrefix("/img/").Handler(fs)

	return router
}

func (s *Serve) Start() {
	go func() {
		httpError := http.ListenAndServe(s.addr, s.router)
		if httpError != nil {
			log.Fatalf("error start serve: %s", httpError)
		}
	}()
}

func (s *Serve) Shutdown() {
	err := s.serverHTTP.Shutdown(context.Background())
	if err != nil {
		log.Printf("http shutdown error %s", err)
		return
	}
	log.Printf("stop http server: %s", s.addr)
}

func logRequest(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)
		log.Printf(
			"%s %s %s",
			r.Method,
			r.RequestURI,
			time.Since(start),
		)
	})
}
