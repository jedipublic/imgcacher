package main

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestCacher(t *testing.T) {
	var tests = []struct {
		name         string
		functionMake Func
		functionDel  FuncDone
		wantErr      bool
	}{
		{
			name: "string",
			functionMake: func(key string) (i interface{}, e error) {
				return "ok", nil
			},
			functionDel: func(value interface{}) {

			},
			wantErr: false,
		},
		{
			name: "error",
			functionMake: func(key string) (i interface{}, e error) {
				return nil, fmt.Errorf("error with key: %s", key)
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			memoCache := NewMemo(context.TODO(), time.Second, time.Second)
			_, err := memoCache.Get("a", tt.functionMake, tt.functionDel)
			if err != nil && !tt.wantErr {
				t.Errorf("error: %s", err)
			}
		})
	}
}
