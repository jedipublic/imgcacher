package main

import (
	"log"
	"os"
	"sync"
	"time"
)

type Item struct {
	FileNmae string
	File     *os.File
	expires  *time.Time
	sync.RWMutex
}

func (i *Item) touch(duration time.Duration) {
	i.Lock()
	expiration := time.Now().Add(duration)
	i.expires = &expiration
	i.Unlock()
}

func (i *Item) Expired() bool {
	var value bool
	i.RLock()
	if i.expires == nil {
		value = true
	} else {
		value = i.expires.Before(time.Now())
	}

	if value {
		fileName := i.File.Name()
		err := os.Remove(fileName)
		if err != nil {
			log.Printf("Unable to remove file: %s", fileName)
		}
	}

	i.RUnlock()
	return value
}
