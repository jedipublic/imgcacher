package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	configDefaultPORT     = 8081
	defaultTimeExpiration = time.Hour
)

type APP struct {
	osSig chan os.Signal
}

func App(osSig chan os.Signal) *APP {
	return &APP{
		osSig: osSig,
	}
}

func (a APP) Start() {
	ctx, cancel := context.WithCancel(context.Background())

	memoCache := NewMemo(ctx, defaultTimeExpiration, time.Second*2)
	caseUse := NewUsecase(memoCache)
	handlers := NewHandlers(caseUse, defaultTimeExpiration)

	webServer := NewServe(configDefaultPORT,
		Route{
			Name:        "resize",
			Method:      http.MethodGet,
			Pattern:     "/image",
			HandlerFunc: handlers.HandlePicture,
		},
	)

	webServer.Start()
	log.Print("service started")
	<-a.osSig

	cancel()
	webServer.Shutdown()
	log.Print("service stopped")
}
